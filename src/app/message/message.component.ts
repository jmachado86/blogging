import {Component, OnChanges, OnInit} from '@angular/core';
import {MessageService} from '../services/message.service';
import {MatDialog, MatSnackBar} from '@angular/material';
import {DialogComponent} from '../dialog/dialog.component';
import {FormControl, Validators} from '@angular/forms';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit, OnChanges {

  messages = [];
  content = '';
  message = new FormControl('', [Validators.required]);

  constructor(private messageService: MessageService,
              public snackBar: MatSnackBar,
              public dialog: MatDialog) { }

  ngOnInit() {
    this.getGetMessages();
  }

  ngOnChanges() {
    this.getGetMessages();
  }

  getGetMessages(): void {
    this.messageService.getMessages().subscribe((response) => this.messages = response);
  }

  deleteMessage(id): void {
    this.messageService.deleteMessage(id).subscribe((response) =>
      this.openSnackBar( 'Message successfully deleted', ''));
  }

  saveMessage(content): void {
    this.messageService.saveMessage({ content: content}).subscribe((response) => {
      this.content = '';
      this.openSnackBar('Message successfully Saved', '');
    });
  }

  openDialog(id): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '250px',
      data: { id: id }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.deleteMessage(result);
    });
  }

  openSnackBar(message: string, action: string) {
    this.snackBar.open(message, action, {
      duration: 2000,
    });
  }


  getErrorMessage() {
    return this.message.hasError('required') ? 'You must enter a value' :
        '';
  }
}

