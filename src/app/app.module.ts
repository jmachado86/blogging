import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MessageComponent } from './message/message.component';
import {
  MatButton,
  MatButtonModule,
  MatCommonModule,
  MatDialogModule, MatFormField, MatFormFieldModule,
  MatGridListModule, MatInput, MatInputModule, MatSnackBar, MatSnackBarModule,
  MatTableModule
} from '@angular/material';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {MessageService} from './services/message.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { DialogComponent } from './dialog/dialog.component';
import {FormsModule} from '@angular/forms';
import {MomentModule} from 'angular2-moment';

@NgModule({
  declarations: [
    AppComponent,
    MessageComponent,
    DialogComponent
  ],
  imports: [
    BrowserModule,
    MatTableModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatGridListModule,
    MatCommonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MomentModule
  ],
  providers: [MessageService],
  bootstrap: [AppComponent],
  entryComponents: [
    DialogComponent
  ]
})
export class AppModule { }
