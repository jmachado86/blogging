import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/index';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  public baseUrl = 'https://bloginwebapi.azurewebsites.net';

  constructor(private http: HttpClient) { }

  getMessages() : Observable<any> {
    return this.http.get(`${this.baseUrl}/Api/Message`);
  }

  saveMessage(message) : Observable<any> {
    return this.http.post(`${this.baseUrl}/Api/Message`, message);
  }

  deleteMessage(id): Observable<any> {
    return this.http.delete(`${this.baseUrl}/Api/Message/${id}`);
  }
}
