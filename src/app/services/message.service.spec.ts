import { TestBed, inject } from '@angular/core/testing';

import { MessageService } from './message.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('MessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MessageService]
    });
  });

  it('should be created', inject([MessageService], (service: MessageService) => {
    expect(service).toBeTruthy();
  }));

  it('should return a list of messages', inject([MessageService, HttpTestingController],
    (service: MessageService, http: HttpTestingController) => {
    let messages: any[] = [];
    service.getMessages().subscribe(result => messages = result);
    const mock = http.expectOne(`${service.baseUrl}/Api/Message`);
    mock.flush({});
    http.verify();
    expect(mock.request.method).toBe('GET');
    expect(service.getMessages()).not.toBeNull();
    expect(messages).not.toBe([]);
  }));

  it('should save a message', inject([MessageService, HttpTestingController],
    (service: MessageService, http: HttpTestingController) => {
      let result = null;
      service.saveMessage({ content: 'test'}).subscribe(response => result = response);
      const mock = http.expectOne(`${service.baseUrl}/Api/Message`);
      mock.flush({ model: true});
      http.verify();
      const requestBody = mock.request.body;
      expect(mock.request.method).toBe('POST');
      expect(result.model).toBeTruthy();
  }));

  it('should delete a message', inject([MessageService, HttpTestingController],
    (service: MessageService, http: HttpTestingController) => {
      let result = null;
      service.deleteMessage(1).subscribe(response => result = response);
      const mock = http.expectOne(`${service.baseUrl}/Api/Message/1`);
      mock.flush({ model: true});
      http.verify();
      const requestBody = mock.request.body;
      expect(mock.request.method).toBe('DELETE');
      expect(result.model).toBeTruthy();
    }));

});
